import express from 'express';
import morgan from 'morgan';
import session = require('express-session');
import { Express } from 'express';
import bodyParser from 'body-parser';
import * as dotenv from 'dotenv';
import router from './routes';

import cookieParser from 'cookie-parser';

dotenv.config();
const sessionMiddleware = session({
  // genid: req => uuidv4(),
  secret: 'keyboard cat',
  resave: false,
  saveUninitialized: true,
  cookie: {},
});

const app: Express = express();
const port = process.env.PORT;

app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(sessionMiddleware);
app.use(router);

app.listen(port, () => {
  console.log(`⚡️[server]: Server is running at http://localhost:${port}`);
});
