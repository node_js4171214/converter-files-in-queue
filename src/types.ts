import { Express } from 'express';
import { AvailableFormatInfo, FormatEnum } from 'sharp';

export type Format = keyof FormatEnum | AvailableFormatInfo;

export type SessionFile = {
  file: Express.Multer.File;
  status: 'success' | 'error' | 'in-progress';
  outputFilename?: string;
  outputFormat?: Format;
};

export interface ConvertFile {
  (value: { file: Express.Multer.File; format: Format }): Promise<{
    outputFilename: string;
    outputFormat: Format;
  }>;
}
