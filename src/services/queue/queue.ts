type OptionsQueue<T> = {
  doneHandler: (value: {
    status: 'error' | 'success';
    task: T;
    error?: Error;
    data?: unknown;
  }) => void;
  handler: (task: T, cb: (error: Error | null, data?: unknown) => void) => void;
};

export class Queue<T> {
  private doneHandler: OptionsQueue<T>['doneHandler'];
  private handler: OptionsQueue<T>['handler'];
  private concurrency: number;
  private count = 0;
  private waiting: T[] = [];
  constructor(concurrency: number, options: OptionsQueue<T>) {
    this.doneHandler = options.doneHandler;
    this.handler = options.handler;
    this.concurrency = concurrency;
  }

  add(task: T) {
    if (this.count < this.concurrency) {
      return this.next(task);
    }

    this.waiting.push(task);
  }

  private next(task: T) {
    this.count += 1;

    this.handler(task, (error, data) => {
      if (error) {
        this.doneHandler({ status: 'error', task, error });
      } else {
        this.doneHandler({ status: 'success', task, data });
      }
      this.count--;
      if (this.waiting.length > 0) {
        const task = this.waiting.shift()!;

        this.next(task);
      }
    });
  }
}
