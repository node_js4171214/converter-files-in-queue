import { Queue } from '../queue/queue';
import { convertFile } from './converter';
import { TransformItem } from './types';
import storage from '../../store/converterStore';
import { SessionFile } from '../../types';

const handler = (
  task: TransformItem,
  cb: (error: Error | null, data?: unknown) => void
) => {
  convertFile({ format: task.formatTo, file: task.file })
    .then(value => {
      storage.setOutputValuesToFile(task.userToken, {
        outputFormat: value.outputFormat,
        outputFilename: value.outputFilename,
        fileToken: task.fileToken,
      });
      cb(null, value);
    })
    .catch(err => cb(err));
};

const doneHandler = (value: {
  status: SessionFile['status'];
  task: TransformItem;
  error?: Error;
  data?: unknown;
}) => {
  const userToken = value.task.userToken;
  const fileToken = value.task.fileToken;

  storage.setStatusToFile(userToken, { fileToken, status: value.status });
};

const options = {
  handler,
  doneHandler,
};
export const converterQueue = new Queue(3, options);
