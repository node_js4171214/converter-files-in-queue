import { AvailableFormatInfo, FormatEnum } from 'sharp';
import { Express } from 'express';

export type TransformItem = {
  userToken: string;
  fileToken: string;
  filepath?: string;
  formatFrom?: string;
  formatTo: keyof FormatEnum | AvailableFormatInfo;
  file: Express.Multer.File;
  quality?: number;
};
