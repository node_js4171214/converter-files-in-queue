import { Request, Response } from 'express';
import storage from '../../store/converterStore';

export function StatusController(req: Request, res: Response) {
  const fileId = req.params.id;
  // const userToken = req.cookies?.token;
  const userToken = (req.headers?.token as string) || '';
  if (!userToken) {
    res.json({ err: 'could not check status ' });
  }

  const status = storage.getStatusFile({ userToken, fileToken: fileId });

  if (status) {
    res.json({
      status: status,
    });
  } else {
    res.json({ error: 'files not found' });
  }
}
