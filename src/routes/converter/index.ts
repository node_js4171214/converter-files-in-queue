import express from 'express';
import multer from 'multer';

import { UploadController } from './upload.controller';
import { DownloadController } from './download.controller';
import { StatusController } from './status.controller';
import { MULTER_DEST } from '../constants';

const router = express.Router();
const upload = multer({ dest: MULTER_DEST });

router.post('/converter/upload', upload.array('files'), UploadController);

router.get('/converter/download/:fileId', DownloadController);

router.get('/converter/status/:id', StatusController);

export default router;
