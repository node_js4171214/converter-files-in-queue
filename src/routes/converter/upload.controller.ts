import { Express, Request, Response } from 'express';
import { nanoid } from 'nanoid';
import storage from '../../store/converterStore';
import { TransformItem } from '../../services/converter/types';
import { converterQueue } from '../../services/converter/converterQueue';

export async function UploadController(req: Request, res: Response) {
  const userToken = req.cookies?.token || nanoid();
  res.cookie('token', userToken, { maxAge: 3 * 60 * 60 });

  const files = req.files as Express.Multer.File[];

  if (files.length) {
    const newFiles = storage.addFiles(userToken, files);
    const fileIdList = Object.keys(newFiles);

    for (const [fileId, file] of Object.entries(newFiles)) {
      const task: TransformItem = {
        userToken,
        fileToken: fileId,
        formatTo: req.body.outputFormat,
        file: file.file,
      };

      converterQueue.add(task);
    }

    res.json({ fileIdList });
  } else {
    res.json({ error: 'files not found' });
  }
}
