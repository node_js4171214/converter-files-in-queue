import express from 'express';
import converterRoutes from './converter';

const router = express.Router();

router.get('/', (req, res) => {
  res.send('server is working!!!');
});

router.use(converterRoutes);

export default router;
