import session from 'express-session';
import { Express } from 'express';
import { SessionFiles } from './types';

declare module 'express-session' {
  interface SessionData {
    queue: {
      userId: string;
      files: SessionFiles;
    }[];
  }
}
